// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package Proto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// GetRecordsClient is the client API for GetRecords service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type GetRecordsClient interface {
	GetRecordsByProto(ctx context.Context, in *GetRecordRequest, opts ...grpc.CallOption) (*GetRecordResponseList, error)
}

type getRecordsClient struct {
	cc grpc.ClientConnInterface
}

func NewGetRecordsClient(cc grpc.ClientConnInterface) GetRecordsClient {
	return &getRecordsClient{cc}
}

func (c *getRecordsClient) GetRecordsByProto(ctx context.Context, in *GetRecordRequest, opts ...grpc.CallOption) (*GetRecordResponseList, error) {
	out := new(GetRecordResponseList)
	err := c.cc.Invoke(ctx, "/getRecords.GetRecords/GetRecordsByProto", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// GetRecordsServer is the server API for GetRecords service.
// All implementations must embed UnimplementedGetRecordsServer
// for forward compatibility
type GetRecordsServer interface {
	GetRecordsByProto(context.Context, *GetRecordRequest) (*GetRecordResponseList, error)
	mustEmbedUnimplementedGetRecordsServer()
}

// UnimplementedGetRecordsServer must be embedded to have forward compatible implementations.
type UnimplementedGetRecordsServer struct {
}

func (UnimplementedGetRecordsServer) GetRecordsByProto(context.Context, *GetRecordRequest) (*GetRecordResponseList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetRecordsByProto not implemented")
}
func (UnimplementedGetRecordsServer) mustEmbedUnimplementedGetRecordsServer() {}

// UnsafeGetRecordsServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to GetRecordsServer will
// result in compilation errors.
type UnsafeGetRecordsServer interface {
	mustEmbedUnimplementedGetRecordsServer()
}

func RegisterGetRecordsServer(s grpc.ServiceRegistrar, srv GetRecordsServer) {
	s.RegisterService(&GetRecords_ServiceDesc, srv)
}

func _GetRecords_GetRecordsByProto_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetRecordRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GetRecordsServer).GetRecordsByProto(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/getRecords.GetRecords/GetRecordsByProto",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GetRecordsServer).GetRecordsByProto(ctx, req.(*GetRecordRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// GetRecords_ServiceDesc is the grpc.ServiceDesc for GetRecords service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var GetRecords_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "getRecords.GetRecords",
	HandlerType: (*GetRecordsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetRecordsByProto",
			Handler:    _GetRecords_GetRecordsByProto_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "grpc/proto/getRecords.proto",
}
