package server

import (
	"context"
	"log"
	"net"
	"newbloodproject/config"
	pb "newbloodproject/grpc/proto"
	db "newbloodproject/model"
	"newbloodproject/utils"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	pb.UnimplementedGetRecordsServer
}

// 定義服務端method
func (s *Server) GetRecordsByProto(ctx context.Context, req *pb.GetRecordRequest) (*pb.GetRecordResponseList, error) {
	// 從DB取得登入紀錄
	recordsFromDB := db.FindRecordByAccount(req.Account)
	if len(recordsFromDB) == 0 {
		err := status.Error(codes.NotFound, "No records were found.")
		return nil, err
	}

	// 初始化 GetRecordResponse的slice
	records := []*pb.GetRecordResponse{}

	// 把recordsFromDB的資料取出並放入records
	for _, record := range recordsFromDB {

		records = append(records, &pb.GetRecordResponse{
			Account: record.Account,
			UserId:  record.UserId,

			// 時間戳必須依照定義的時間設定
			LoginTime: record.Login.Format("2006-01-02 15:04:05"),
		})

	}

	return &pb.GetRecordResponseList{Records: records}, nil

}

// 建立 gRPC 服務端
func ProtoServer() {
	// 設置環境變數
	config.EnvInit()

	log.Println("Listening gRPC on port 5001.")

	// 監聽 port 5001.
	lis, err := net.Listen("tcp", "localhost:5001")

	utils.CheckError(err)
	// 啟動伺服器端
	s := grpc.NewServer()

	// 以Server註冊新的伺服器端
	pb.RegisterGetRecordsServer(s, &Server{})

	// 開始服務port 5001
	if err := s.Serve(lis); err != nil {
		log.Fatal(err)
	}

}
