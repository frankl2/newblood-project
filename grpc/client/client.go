package client

import (
	"log"
	"net/http"
	pb "newbloodproject/grpc/proto"
	"newbloodproject/utils"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

func GetRecords(ctx *gin.Context) {
	// 取得query param
	account := ctx.Query("account")

	// 定義要連接到伺服器的port及選項
	conn, err := grpc.Dial("localhost:5001", grpc.WithInsecure())

	utils.CheckError(err)

	// 建立新的連線
	c := pb.NewGetRecordsClient(conn)

	// 向伺服器發送請求及接收回應
	response, err := c.GetRecordsByProto(ctx, &pb.GetRecordRequest{Account: account})

	if err != nil {
		status, _ := status.FromError(err)

		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": status.Message(),
		})
		ctx.Abort()
		return
	}

	log.Printf("%v", response)

	ctx.JSON(http.StatusOK, gin.H{
		"result": response,
	})
}
