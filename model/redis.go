package db

import (
	"context"
	"encoding/json"
	"log"
	"newbloodproject/utils"
	"time"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

var rdb = RedisInit()

func RedisInit() *redis.Client {
	// 建立新的連線
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:8050",
		Password: "redis1234",
		DB:       0,
	})

	return rdb
}

func AddUserToRedis(user utils.User) {
	// 將JSON轉換成string
	jsonUser, err := json.Marshal(user)

	utils.CheckError(err)

	// 寫入Redis，紀錄過期時效為20秒
	err = rdb.Set(ctx, user.Id, jsonUser, 20*time.Second).Err()

	utils.CheckError(err)

	log.Println("Successfully add new user to redis.")

}

func FindUserByIdR(userId string) utils.User {
	var user utils.User

	// 從Redis找到使用者
	jsonUser, err := rdb.Get(ctx, userId).Result()

	// 如果使用者找到
	if err != redis.Nil {

		utils.CheckError(err)
		// 將JSON轉換為string
		json.Unmarshal([]byte(jsonUser), &user)
	}

	return user
}
