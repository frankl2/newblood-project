package db

import (
	"database/sql"
	"fmt"
	"log"
	"newbloodproject/utils"
	"os"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/joho/godotenv"
)

// DB連線
func connectDB() *sql.DB {

	var (
		driverName = os.Getenv("DRIVER_NAME")
		userName   = os.Getenv("USER_NAME_SQL")
		password   = os.Getenv("PASSWORD_SQL")
		dbURI      = os.Getenv("DB_URI")
		dbName     = os.Getenv("DB_NAME")
	)

	// 連線至MySQL
	db, err := sql.Open(driverName, fmt.Sprintf("%s:%s@%s/%s?parseTime=true", userName, password, dbURI, dbName))
	utils.CheckError(err)

	// 測試連線是否正常
	err = db.Ping()
	utils.CheckError(err)

	log.Println("Successfully connect to database.")
	return db

}

func AddUserToDB(user utils.User) {

	db := connectDB()

	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO users (id, account, password)VALUES (?,?,?);")
	utils.CheckError(err)

	result, err := stmt.Exec(
		user.Id,
		user.Account,
		user.Password)
	utils.CheckError(err)

	log.Println("Successfully add new user.-------", result)
}

// 更新使用者的Token
func AddTokenById(token, userId string) {

	db := connectDB()

	defer db.Close()
	stmt, err := db.Prepare("UPDATE users SET token=? WHERE id=?")
	utils.CheckError(err)
	result, err := stmt.Exec(token, userId)
	utils.CheckError(err)
	log.Println(result)

}

// 新增登入紀錄
func AddRecordById(userId string) {

	db := connectDB()

	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO user_records(user_id) VALUES (?);")
	utils.CheckError(err)

	result, err := stmt.Exec(userId)
	utils.CheckError(err)
	log.Println(result)
}

// 清除使用者Token
func DeleteTokenById(userId string) {

	db := connectDB()

	defer db.Close()

	stmt, err := db.Prepare("UPDATE users SET token=? WHERE id=?;")
	utils.CheckError(err)
	result, err := stmt.Exec(0, userId)

	utils.CheckError(err)

	log.Println(result)
}

// 確認使用者資料是否正確
func ShouldLogin(user utils.UserLogin) utils.User {
	db := connectDB()

	defer db.Close()

	rows, err := db.Query("SELECT * FROM users WHERE account=? AND password=? ;", user.Account, user.Password)
	utils.CheckError(err)

	var userStr utils.User

	for rows.Next() {
		err := rows.Scan(&userStr.Id, &userStr.Account, &userStr.Password, &userStr.Token)
		utils.CheckError(err)

	}
	return userStr
}

func FindUserById(userId string) utils.User {
	db := connectDB()

	defer db.Close()

	rows, err := db.Query("SELECT * FROM users WHERE id=? ", userId)
	utils.CheckError(err)

	var userStr utils.User

	for rows.Next() {
		err := rows.Scan(&userStr.Id, &userStr.Account, &userStr.Password, &userStr.Token)
		utils.CheckError(err)
	}
	return userStr
}

func FindUserByAccount(userAccount string) utils.User {
	db := connectDB()

	defer db.Close()

	rows, err := db.Query("SELECT * FROM users WHERE account=? ", userAccount)
	utils.CheckError(err)

	var userStr utils.User

	for rows.Next() {
		err := rows.Scan(&userStr.Id, &userStr.Account, &userStr.Password, &userStr.Token)
		utils.CheckError(err)
	}
	return userStr
}

// 檢查重複帳號
func CheckByUserAccount(userAccount string) utils.User {
	db := connectDB()

	defer db.Close()

	var userStr utils.User

	rows, err := db.Query("SELECT * FROM users WHERE account=?;", userAccount)
	utils.CheckError(err)

	for rows.Next() {
		err := rows.Scan(&userStr.Id, &userStr.Account, &userStr.Password, &userStr.Token)
		utils.CheckError(err)
	}

	return userStr
}

func UpdateUserProfile(userId, account, password string) {
	db := connectDB()
	defer db.Close()

	stmt, err := db.Prepare("UPDATE users SET account=?,password=? WHERE id=?")
	utils.CheckError(err)

	result, err := stmt.Exec(account, password, userId)
	utils.CheckError(err)
	log.Println(result)

}

func DeleteRecordById(userId string) {

	db := connectDB()
	defer db.Close()

	stmt, err := db.Prepare("DELETE FROM user_records WHERE user_id=?")
	utils.CheckError(err)

	result, err := stmt.Exec(userId)
	utils.CheckError(err)

	log.Println(result)
}

func FindRecordById(userId string) utils.LoginRecord {
	db := connectDB()
	defer db.Close()
	var record utils.LoginRecord
	rows, err := db.Query(
		"SELECT user_records.login_time, user_records.user_id, users.account FROM user_records LEFT JOIN users ON users.id=user_records.user_id WHERE user_records.user_id=?;",
		userId,
	)
	utils.CheckError(err)
	rows.Scan(&record.Id, &record.Login, &record.UserId)
	return record
}

func FindRecordByAccount(account string) []utils.LoginRecord {
	db := connectDB()
	defer db.Close()
	var record utils.LoginRecord
	var records []utils.LoginRecord
	rows, err := db.Query(
		"SELECT user_records.id,  user_records.user_id, users.account, user_records.login_time FROM user_records LEFT JOIN users ON users.id=user_records.user_id WHERE account=?;",
		account,
	)
	utils.CheckError(err)

	for rows.Next() {
		err = rows.Scan(&record.Id, &record.UserId, &record.Account, &record.Login)
		utils.CheckError(err)
		records = append(records, record)
	}

	return records
}

func FindAllUser() []utils.User {
	db := connectDB()
	defer db.Close()

	var user utils.User
	var users []utils.User
	rows, err := db.Query("SELECT * FROM users;")
	utils.CheckError(err)

	for rows.Next() {
		err := rows.Scan(&user.Account, &user.Id, &user.Password, &user.Token)
		utils.CheckError(err)
		users = append(users, user)
	}

	return users
}
