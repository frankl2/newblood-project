package middleware

import (
	"net/http"
	"newbloodproject/auth"
	"strings"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware() func(c *gin.Context) {

	return func(c *gin.Context) {

		header := c.Request.Header.Get("Authorization")

		// 將JWT分成 Bearer 及 token 字串
		tokenParts := strings.Split(header, " ")

		// 如果沒有提供token
		if header == "" {
			c.JSON(http.StatusUnauthorized, gin.H{
				"error": "Not authorized, no token.",
			})
			c.Abort()
			return

			// 如果提供的token開頭不是Bearer
		} else if len(tokenParts) == 2 && tokenParts[0] != "Bearer" {
			c.JSON(http.StatusUnauthorized, gin.H{
				"error": "Not authorized, wrong token.",
			})
			c.Abort()
			return

		} else {
			// 解析token
			token, err := auth.ParseToken(tokenParts[1])

			// 如果解析失敗則回傳錯誤訊息
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error": err.Error(),
				})
				c.Abort()
				return

			} else {

				// 用解析出來的userId在去資料庫找到使用者，並將參數傳入Context
				userId := token.Id

				c.Set("userId", userId)

			}

		}
	}
}
