package utils

import (
	"time"
)

type User struct {
	Id       string
	Account  string `json:"account" binding:"required" validate:"isExist"`
	Password string `json:"password" binding:"required"`
	Token    string `json:"token"`
}

type UserLogin struct {
	Account  string `json:"account" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type UserEdit struct {
	Account            string `json:"account" binding:"required"`
	Password           string `json:"password" binding:"required"`
	NewPassword        string `json:"newPassword" binding:"nefield=Password"  `
	ConfirmNewPassword string `json:"confirmNewPassword" binding:"eqfield=NewPassword"`
}

type LoginRecord struct {
	Account string
	Id      int32
	Login   time.Time
	UserId  string
}
