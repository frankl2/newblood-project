package utils

import (
	"log"
)

func KeepRunning() {
	select {}
}

func CheckError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
