package routes

import (
	"newbloodproject/controller"
	"newbloodproject/grpc/client"
	"newbloodproject/middleware"

	"github.com/gin-gonic/gin"
)

func Routes(r *gin.Engine) {
	route := r.Group("/api")
	{
		// 註冊
		route.POST("/register", controller.UserRegister)
		// 登入
		route.POST("/login", controller.UserLogin)
		// 查詢登入紀錄
		route.GET("/login", middleware.AuthMiddleware(), client.GetRecords)
		// 登出
		route.POST("/logout", middleware.AuthMiddleware(), controller.UserLogout)

	}

	userRoute := r.Group("/api/user")
	{
		// 查詢使用者資料
		userRoute.GET("/:account", middleware.AuthMiddleware(), controller.GetUserProfile)
		// 更新使用者資料
		userRoute.POST("/", middleware.AuthMiddleware(), controller.EditUserProfile)
		// 清除登入紀錄
		userRoute.DELETE("/", middleware.AuthMiddleware(), controller.ClearLoginRecord)
	}
	// 開始監聽port 5000
	r.Run(":5000")
}
