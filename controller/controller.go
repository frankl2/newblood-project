package controller

import (
	"net/http"
	"newbloodproject/auth"
	db "newbloodproject/model"
	"newbloodproject/utils"
	"newbloodproject/validate"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

// @desc 使用者註冊
// @route POST /api/register
// @access Public
func UserRegister(c *gin.Context) {
	var reqBody utils.User

	// 將request body綁到reqBody
	if err := c.ShouldBindJSON(&reqBody); err != nil {

		// 建立翻譯器
		// 將err轉換成ValidationErrors類型
		trans := validate.ErrorTranslate()
		errs := err.(validator.ValidationErrors)

		for _, err := range errs {
			// 回傳客戶端錯誤訊息。 用trans當作翻譯器翻譯err的錯誤訊息
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Translate(trans),
			})
			return
		}
	}

	// 檢查用戶名是否已被使用
	valiErr := validate.MyValidate(reqBody)
	// 如被使用則回傳錯誤訊息並撤出
	if valiErr != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Account has been taken.",
		})

		return
	}

	// 使用者密碼加密
	reqBody.Password = auth.PasswordEncrypt(reqBody.Password)

	// 生成Id
	reqBody.Id = uuid.New().String()

	// 把使用者資料寫入SQL跟Redis
	db.AddUserToDB(reqBody)
	db.AddUserToRedis(reqBody)

	c.JSON(201, gin.H{
		"message": "Account is created successfully.",
	})

}

// @desc 使用者登入
// @route POST /api/login
// @access Public
func UserLogin(c *gin.Context) {

	var reqBody utils.UserLogin
	var record utils.LoginRecord

	if err := c.ShouldBindJSON(&reqBody); err != nil {

		// 建立翻譯器
		// 將err轉換成ValidationErrors類型
		trans := validate.ErrorTranslate()
		errs := err.(validator.ValidationErrors)

		for _, err := range errs {
			// 回傳客戶端錯誤訊息。 用trans當作翻譯器翻譯err的錯誤訊息
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Translate(trans),
			})
			return
		}

	}

	// 將reqBody裡面的Password加密
	reqBody.Password = auth.PasswordEncrypt(reqBody.Password)

	// 與DB內的帳號密碼比對是否正確
	user := db.ShouldLogin(reqBody)

	// 如果user存在
	if user != (utils.User{}) {

		// 生成JWT及時間戳
		record.UserId = user.Id
		token, _ := auth.GenToken(user.Id)
		record.Login = time.Now()

		// 寫入 table `user_records`
		db.AddRecordById(record.UserId)

		// 更新 table `users`的Token欄位
		db.AddTokenById(token, record.UserId)

		c.JSON(http.StatusOK, gin.H{
			"account": reqBody.Account,
			"token":   "Bearer " + token,
		})

	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid account or password.",
		})
	}

}

// @desc 使用者登出
// @route POST /api/loguot
// @access Private
func UserLogout(c *gin.Context) {

	// 從context裡面取得userId
	userId := c.MustGet("userId")

	// 更新 table `users`的Token欄位成預設值
	db.DeleteTokenById(userId.(string))

	c.JSON(http.StatusOK, gin.H{
		"message": "Successfully logout!",
	})

}

// @desc 查詢使用者資料
// @route GET /api/user/:account
// @access Private
func GetUserProfile(c *gin.Context) {

	userAccount := c.Param("account")
	userId := c.MustGet("userId").(string)
	var user utils.User

	// 先從Redis內找該用戶資料，如沒有則到MySQL內找，並重新寫入Redis
	user = db.FindUserByIdR(userId)
	if user == (utils.User{}) {
		user = db.FindUserById(userId)
		db.AddUserToRedis(user)
	}

	// 如果用戶找到且提供帳號為本人
	if user != (utils.User{}) && userAccount == user.Account {
		c.JSON(http.StatusOK, gin.H{
			"Id":      user.Id,
			"Account": user.Account,
			"Token":   user.Token,
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"message": "User does not exist.",
		})
	}

}

// @desc 修改使用者資料
// @route POST /api/user/
// @access Private
func EditUserProfile(c *gin.Context) {

	var reqBody utils.UserEdit
	var userId = c.MustGet("userId").(string)

	if err := c.ShouldBindJSON(&reqBody); err != nil {

		// 建立翻譯器
		// 將err轉換成ValidationErrors類型
		trans := validate.ErrorTranslate()
		errs := err.(validator.ValidationErrors)

		for _, err := range errs {
			// 回傳客戶端錯誤訊息。 用trans當作翻譯器翻譯err的錯誤訊息
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Translate(trans),
			})
			return
		}

	}

	// 將密碼加密與資料庫內的密碼做比對
	encryptPassword := auth.PasswordEncrypt(reqBody.Password)
	user := db.FindUserById(userId)

	//如果該使用者存在
	if user != (utils.User{}) {

		// 且提供的密碼正確
		if encryptPassword == user.Password {

			// 如果有提供新密碼，將新密碼加密並更新MySQL內的資料
			if reqBody.NewPassword != "" {

				newPassword := auth.PasswordEncrypt(reqBody.NewPassword)
				db.UpdateUserProfile(userId, reqBody.Account, newPassword)

			} else {

				// 如果沒有提供新密碼，則以舊密碼更新
				db.UpdateUserProfile(userId, reqBody.Account, user.Password)
			}

			c.JSON(http.StatusOK, gin.H{
				"message": "Successfully updated.",
			})

		} else {
			c.JSON(http.StatusUnauthorized, gin.H{
				"error": "Invalid account or password.",
			})
		}

	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "User does not exist.",
		})
	}

}

// @desc 清除使用者登入紀錄
// @route DELETE /api/user/
// @access Private
func ClearLoginRecord(c *gin.Context) {

	type userAccount struct {
		Account string `json:"account" binding:"required"`
	}

	var reqBody userAccount

	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := db.FindUserByAccount(reqBody.Account)

	//如使用者存在，刪除該使用者的登入紀錄
	if user != (utils.User{}) {

		db.DeleteRecordById(user.Id)

		c.JSON(http.StatusOK, gin.H{
			"message": "Successfully delete records.",
		})

	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "User does not exist.",
		})
	}

}
