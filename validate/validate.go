package validate

import (
	db "newbloodproject/model"
	"newbloodproject/utils"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
)

// 建立驗證器
func MyValidate(user utils.User) error {

	validate := validator.New()
	// 以IsExist註冊驗證器並命名為 isExist
	validate.RegisterValidation("isExist", IsExist)
	err := validate.Struct(user)

	return err
}

// 檢查用戶名是否被使用
func IsExist(fl validator.FieldLevel) bool {
	account := fl.Field().String()
	ExistUser := db.FindUserByAccount(account)

	// 已被使用為false, 未被使用為true
	return ExistUser == (utils.User{})
}

// 註冊，建立並回傳客製翻譯器
func ErrorTranslate() ut.Translator {
	var trans ut.Translator

	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {

		// 建立翻譯器
		en := en.New()
		uni := ut.New(en, en)
		trans, _ = uni.GetTranslator("en")

		// 註冊翻譯器
		transMessage(v, trans)
	}
	return trans
}

//	定義翻譯訊息並註冊到validator
func transMessage(v *validator.Validate, trans ut.Translator) {

	// 註冊翻譯訊息到 universal-translator
	registerFn := func(ut ut.Translator) error {

		// {0} 對應到 fe.Field()
		return ut.Add("required", "{0} is required.", false)

	}
	//	定義翻譯格式
	translateFn := func(ut ut.Translator, fe validator.FieldError) string {
		//
		field := fe.Field()
		tag := fe.Tag()

		t, _ := ut.T(tag, field)

		return t
	}

	// 將翻譯所需參數傳入並註冊到validator
	v.RegisterTranslation("required", trans, registerFn, translateFn)
}
