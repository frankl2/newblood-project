CREATE DATABASE userDB;

USE userDB;

CREATE TABLE users (
    id VARCHAR(255) NOT NULL PRIMARY KEY,
    account VARCHAR(30) NOT NULL,
    password VARCHAR(255) NOT NULL,
    token VARCHAR(255) NOT NULL DEFAULT 0
);

CREATE TABLE user_records (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    login_time TIMESTAMP DEFAULT NOW(),
    user_id VARCHAR(255) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id) 
);

