package auth

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type MyClaim struct {
	Id string
	jwt.StandardClaims
}

var jwtKey = os.Getenv("JWT_SECRET")
var secretKey = []byte(jwtKey)
var myCalim = MyClaim{}

// MD5加密
func PasswordEncrypt(password string) string {
	hash := md5.New()
	hash.Write([]byte(password))
	encrypted := hex.EncodeToString(hash.Sum(nil))
	return encrypted
}

// 用傳入的user id生成token
func GenToken(userId string) (string, error) {
	// 設定token5分鐘後過期
	tokenExpired := time.Minute * 5

	claim := MyClaim{
		userId,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(tokenExpired).Unix(),
			Issuer:    "ginAPI",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	return token.SignedString(secretKey)

}

// 解析token，成功回傳user id。
func ParseToken(tokenString string) (*MyClaim, error) {

	token, err := jwt.ParseWithClaims(
		tokenString,
		&myCalim,
		func(token *jwt.Token) (interface{}, error) {
			return secretKey, nil
		})

	if err != nil {

		return nil, err
	}

	if claim, ok := token.Claims.(*MyClaim); ok && token.Valid {

		return claim, nil
	}

	return nil, errors.New("token failed")
}
