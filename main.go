package main

import (
	"newbloodproject/config"
	"newbloodproject/grpc/server"
	"newbloodproject/routes"
	"newbloodproject/utils"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv"
)

func main() {

	config.EnvInit()

	r := gin.Default()
	//  RESTful 服務
	go routes.Routes(r)
	// gRPC 服務
	go server.ProtoServer()
	// 兩個服務持續進行
	utils.KeepRunning()
}
