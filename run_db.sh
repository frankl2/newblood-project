#! /bin/bash

function composeUp() {
docker compose --env-file config/.env up -d 
    
}

function composeDown() {
    docker compose --env-file config/.env stop
}


# 關閉Docker compose
if  [ $1 == "down" ]
then 
    echo "Closing containers..."
    composeDown
# 啟動Docker compose
elif [ $1 == "up" ]
then
    echo "Running up containers..."
    composeUp
else 
    echo "Unknown command: 'up' to run up, 'down' to stop"
fi









